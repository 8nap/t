FROM openjdk:8-jdk-alpine
MAINTAINER Kiryl Dzerabin
COPY /target/super-project-0.0.1-SNAPSHOT.jar /bin/
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/bin/super-project-0.0.1-SNAPSHOT.jar"]
EXPOSE 8900