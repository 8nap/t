package backend.Controllers;

import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


@RestController
@CrossOrigin
public class WelcomeController {
    ArrayList<String> testArrayList;
    public WelcomeController(){
        testArrayList  = new ArrayList<>(0);
        testArrayList.add("spring");
        testArrayList.add("angular");
        testArrayList.add("microservice architecture");
        testArrayList.add("databases");
        testArrayList.add("openshift");
        testArrayList.add("greeting");
    }
    @GetMapping(value = "/hello")
    public List<String> greeting() {
        return testArrayList;
    }

    @PostMapping(value = "/hello/new")
    public void addNew(@RequestBody String newElement){
        testArrayList.add(newElement);
    }
    @PostMapping(value = "/hello/delete")
    public void removeElement(@RequestBody int id){
        testArrayList.remove(id);
    }

}